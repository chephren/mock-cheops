from consts.numerical_values import HEXADECIMAL_WORD_LENGTH
from utils.hex_generator import generate_hexadecimal
from utils.time_utils import current_milli_time

from .command import Command


class Resource:
    def __init__(self, name: str, commands: list[Command] = None, presences: list[str] = None):

        self.id = generate_hexadecimal(HEXADECIMAL_WORD_LENGTH)
        self.name = name
        self.last_update = current_milli_time()
        self.commands = [] if commands is None else commands
        self.commands_count = len(self.commands)
        self.deleted = False
        self.presences = presences if presences is not None else []

    def __str__(self):
        return f"Resource(id={self.id}, name={self.name}, last_update={self.last_update}, commands={self.commands}, commands_count={self.commands_count}, deleted={self.deleted}, presence={self.presences})"

    def __repr__(self):
        return self.__str__()

    def add_command(self, command: Command):
        if (command.command == "DELETE"):
            self.deleted = True
        self.commands.append(command)
        self.commands_count = len(self.commands)

    @staticmethod
    def from_tuple(data, commands: list[Command] = None, commands_count: int = None, deleted: bool = False, presence: list[str] = None):
        resource = Resource(data[1])
        resource.id = data[0]
        resource.last_update = data[2]
        resource.deleted = deleted
        if presence is not None:
            resource.presences = presence

        if (commands is not None):
            resource.commands = commands
            resource.commands_count = len(resource.commands)
        elif (commands_count is not None):
            resource.commands_count = commands_count
        return resource

    @staticmethod
    def from_full_dict(data: dict):
        resource = Resource(data["name"])
        resource.id = data["id"]
        resource.last_update = data["lastUpdate"]
        resource.deleted = data.get("deleted", False)
        resource.commands = [Command.from_full_dict(
            command) for command in data["commands"]]
        resource.commands_count = len(resource.commands)

        resource.presences = data.get("presence", [])
        return resource

    
    # This method returns a tuple with the following structure:
    # (id, name, last_update)
    # used to insert a resource into the database
    def to_tuple(self):
        return (self.id, self.name, self.last_update)
    
    def to_presence_tuples(self):
        return [(self.id, presence) for presence in self.presences]

    def to_full_dict(self, include_presence: bool = False):
        full_dict = {
            "id": self.id,
            "name": self.name,
            "lastUpdate": self.last_update,
            "deleted": self.deleted,
            "commands": [command.to_dict() for command in self.commands]
        }
        if include_presence:
            full_dict["presence"] = self.presences
        return full_dict

    def to_simple_dict(self, include_presence: bool = False):
        simple_dict = {
            "id": self.id,
            "name": self.name,
            "lastUpdate": self.last_update,
            "deleted": self.deleted,
            "commandsCount": self.commands_count
        }
        if include_presence:
            simple_dict["presence"] = self.presences
        return simple_dict
