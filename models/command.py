from consts.numerical_values import HEXADECIMAL_WORD_LENGTH
from utils.time_utils import current_milli_time
from utils.hex_generator import generate_hexadecimal


class Command:
    def __init__(self, command: str):
        self.id = generate_hexadecimal(HEXADECIMAL_WORD_LENGTH)
        self.command = command
        self.date = current_milli_time()

    def __str__(self):
        return f"Command(id={self.id}, command={self.command}, date={self.date})"

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def from_tuple(data):
        command = Command(data[2])
        command.id = data[0]
        command.date = data[1]
        return command

    @staticmethod
    def from_full_dict(data):
        command = Command(data["command"])
        command.id = data["id"]
        command.date = data["date"]
        return command

    def to_tuple(self, resource_id: str):
        return (resource_id, self.id, self.date, self.command)

    def to_dict(self):
        return {
            "id": self.id,
            "command": self.command,
            "date": self.date
        }
