from utils.database_utils import (clear_command_queue, clear_resource_queue,
                                  get_command_queue, get_resource_queue,
                                  insert_command, insert_presence_bulk, insert_resource)


def fetch_insert_queues(intern_node_address: str):
    """
    Fetches the resources and commands from the queue and inserts them into the database
    """
    fetch_insert_resource_queue(intern_node_address)
    fetch_insert_command_queue(intern_node_address)


def fetch_insert_resource_queue(intern_node_address: str):
    """
    Fetches the resources from the queue and inserts them into the database
    """
    resources = get_resource_queue(intern_node_address, True)
    if (resources is None):
        return
    for resource in resources:
        insert_resource(resource)
        insert_presence_bulk(resource)
        insert_command(resource.id, resource.commands[0] if len(
            resource.commands) > 0 else None)


def fetch_insert_command_queue(intern_node_address: str):
    """
    Fetches the commands from the queue and inserts them into the database
    """
    commands = get_command_queue(intern_node_address, True)
    if (commands is None):
        return
    for command in commands:
        resource_id = command['resource_id']
        cm = command['command']
        insert_command(resource_id, cm)
