import json
import os
import queue

from consts.status import Status


class MessageAnnouncer:
  
  def __init__(self):
    self.listeners = []

  def listen(self):
    q = queue.Queue(maxsize=5)
    self.listeners.append(q)
    q.put_nowait(format_sse(data='Succesfully connected to : ' + os.environ['NODE_FILE']))
    return q
  
  def announce(self, message):
    for i in reversed(range(len(self.listeners))):
      try:
        self.listeners[i].put_nowait(message)
      except queue.Full:
        del self.listeners[i]

announcer = MessageAnnouncer()

def format_sse(data: str, event=None) -> str:
    msg = f'data: {data}\n\n'
    if event is not None:
        msg = f'event: {event}\n{msg}'
    return msg


def send_status_change(node_address: str, status: Status, extra_data: dict = None):
  data = {
    "node_address": node_address,
    "status": status.value
  }
  if extra_data is not None:
    data.update(extra_data)
  data = json.dumps(data)
  
  msg = format_sse(data=data, event='status_change')
  announcer.announce(msg)
