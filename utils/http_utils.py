import json
import sys
import threading
from urllib.parse import urlparse

import requests

from consts.status import Status
from models.command import Command
from models.resource import Resource
from utils.database_utils import (insert_command_queue, insert_resource_queue,
                                  update_state)
from utils.thread_value import Thread_value
from utils.time_utils import RandomNetworkFailure, random_sleep

nodes = {
    'http://localhost:3000/': 'http://node1:3000/',
    'http://localhost:3001/': 'http://node2:3000/',
    'http://localhost:3002/': 'http://node3:3000/',
    'http://localhost:3003/': 'http://node4:3000/',
}

def get_internal_url(port: int):
    # keep last 3 digits
    port = (port % 1000)+1
    new_url = f"http://node{port}:3000/"
    print('getInternalUrl', new_url)
    return new_url



def normalize_url(url: str, inter: bool = False):
    parsed_url = urlparse(url)
    format = parsed_url.scheme + "://" + parsed_url.netloc + "/"
    if inter:
        url = get_internal_url(parsed_url.port | 3000)
        if url is None:
            print('normalize intern', url, file=sys.stderr)
            return
        return url
    else:
        return format
    

def send_data_in_thread(address: str, data: str, source_address: str, on_error_callback: callable = None, external_address: str = None):
    try:
        random_sleep()
        result = requests.put(
            address,
            data=data,
            headers={"Content-Type": "application/json"})
        if result.status_code != 201:
            print('Address', external_address, 'Status code:',
                  result.status_code, file=sys.stderr)
            update_state(Status.UPDATING, source_address, {"message": "Status code not 201", "statusCode": result.status_code, "node_failed": external_address})
            if on_error_callback:
                on_error_callback()
        
    except RandomNetworkFailure as e:
        print('Address (intentional network fail)', external_address, e, file=sys.stderr)
        update_state(Status.UPDATING, source_address, {"message": "Network failure (random intentional)", "statusCode": 418, "node_failed": external_address})
        if on_error_callback:
            on_error_callback()
    except requests.exceptions.RequestException as e:
        print('[RequestException]Address', address, e, file=sys.stderr)
        update_state(Status.UPDATING, source_address, {"message": e, "statusCode": 500, "node_failed": external_address})
        if on_error_callback:
            on_error_callback()
    except requests.exceptions.HTTPError as e:
        print('[HTTPError]Address', address, e, file=sys.stderr)
        update_state(Status.UPDATING, source_address, {"message": e, "statusCode": 500, "node_failed": external_address})
        if on_error_callback:
            on_error_callback()
    except requests.exceptions.Timeout as e:
        print('[Timeout]Address', address, e, file=sys.stderr)
        update_state(Status.UPDATING, source_address, {"message": e, "statusCode": 500, "node_failed": external_address})
        if on_error_callback:
            on_error_callback()
    except Exception as e:
        print('[Exception]Address', address, e, file=sys.stderr)
        update_state(Status.UPDATING, source_address, {"message": 'Exception', "statusCode": 500, "node_failed": external_address})
        if on_error_callback:
            on_error_callback()


def send_resource_to_other_nodes(external_addresses: list[str], resource: Resource, source_address: str):
    '''Send resource to other nodes using threads'''
    all_threads = []
    for address in external_addresses:
        internal_address = normalize_url(address, True)
        data = json.dumps({ "resource": resource.to_full_dict(include_presence=True),})
        on_error_callback = lambda: insert_resource_queue(internal_address, resource)
        th = threading.Thread(target=send_data_in_thread, args=(f"{internal_address}/api/duplicate/resource", data, source_address, on_error_callback, address))
        all_threads.append(th)
        th.start()
    for th in all_threads:
        th.join()


def send_command_to_other_nodes(external_addresses: list[str], resource_id: str, command: Command, source_address: str):
    '''Send command to other nodes using threads'''
    all_threads = []
    for address in external_addresses:
        internal_address = normalize_url(address, True)
        data = json.dumps({ "command": command.to_dict(),})
        on_error_callback = lambda: insert_command_queue(internal_address, resource_id, command)
        th = threading.Thread(target=send_data_in_thread, args=(f"{internal_address}/api/duplicate/resource/{resource_id}/command", data, source_address, on_error_callback, address))
        all_threads.append(th)
        th.start()
    for th in all_threads:
        th.join()


def ping_in_thread(address: str, source_address: str):
    try:
        random_sleep()
        addr = f"{normalize_url(address, True)}/api/ping"
        result = requests.get(addr)
        if result.status_code != 200:
            print('Address', addr, 'Status code:',
                  result.status_code, result.reason, file=sys.stderr)
            update_state(Status.ONLINE, source_address, {"message": "Status code not 200", "statusCode": result.status_code, "node_failed": address})
        return address, result.status_code == 200
    except RandomNetworkFailure as e:
        print('RandomNetworkFailure: Address', address, e, file=sys.stderr)
        update_state(Status.ONLINE, source_address, {"message": "Network failure (random intentional)", "statusCode": 418, "node_failed": normalize_url(address)})
        return address, False
    except requests.exceptions.RequestException as e:
        print('RequestException: Address', addr, e, file=sys.stderr)
        update_state(Status.ONLINE, source_address, {"message": e, "statusCode": 500, "node_failed": address})
        return address, False
    except Exception as e:
        print('Exception: Address', addr, e, file=sys.stderr)
        update_state(Status.ONLINE, source_address, {"message": e, "statusCode": 500, "node_failed": address})
        return address, False


def ping(addresses: list[str], source_address: str):
    all_threads = []
    results = {}
    print('pinging', addresses, flush=True)
    for address in addresses:
        print('ping', address, flush=True)
        th = Thread_value(target=ping_in_thread, args=(address, source_address))
        all_threads.append(th)
        th.start()
    for th in all_threads:
        address, value = th.join()
        results[address] = value
    return results
