from threading import Thread


class Thread_value(Thread):
    def __init__(self, target, args):
        super().__init__(target=target, args=args)
        self._return = None

    def run(self):
        self._return = self._target(*self._args)

    def join(self):
        super().join()
        return self._return