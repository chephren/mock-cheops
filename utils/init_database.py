import os
import random
import sqlite3

from consts.status import Status

from .database_utils import db_file, db_queue_file

node1_address = 'http://localhost:3000/'
node2_address = 'http://localhost:3001/'
node3_address = 'http://localhost:3002/'
node4_address = 'http://localhost:3003/'

status = {
    "node1": {"state": Status.ONLINE.value},
    "node2": {"state": Status.ONLINE.value},
    "node3": {"state": Status.OFFLINE.value},
    "node4": {"state": Status.ONLINE.value},
}

resources = [
    ("746f746f2e747874a", 'Resource 1', 1699272000000),
    ("e539b187571ec476a", 'Resource 2', 1699279200000),
    ("3ad06e1ea9a9c2085", 'Resource 3', 1699272180000),
    ("0eaf0d20311e24abd", 'Resource 4', 1699358580000),
    ("183ca5d3a2f6735bf", 'Resource 5', 1699359580000),
    ("640b0507b542f5854", 'Resource 6', 1699657890000),
]

commands = {
    "746f746f2e747874a": [
        ("746f746f2e747874a", "7363616c616c61", 1699279200000, "mkdir /tmp/test"),
        ("746f746f2e747874a", "7363616c616c62",
         1699279300000, "touch /tmp/test/test.txt"),
        ("746f746f2e747874a", "7363616c616c63", 1699279400000, "rm -rf /tmp/test"),
    ],
    "e539b187571ec476a": [
        ("e539b187571ec476a", "7363616c616c64", 1699279287000, "mkdir /tmp/test2"),
        ("e539b187571ec476a", "7363616c616c65",
         1699279373000, "touch /tmp/test2/test.txt"),
        ("e539b187571ec476a", "7363616c616c66",
         1699279428000, "rm -rf /tmp/test2"),
    ],
    "3ad06e1ea9a9c2085": [
        ("3ad06e1ea9a9c2085", "7363616c616c67", 1699272280000, "mkdir /tmp/test3"),
        ("3ad06e1ea9a9c2085", "7363616c616c68",
         1699272380000, "touch /tmp/test3/test.txt"),
        ("3ad06e1ea9a9c2085", "7363616c616c69",
         1699272480000, "rm -rf /tmp/test3"),
    ],
    "0eaf0d20311e24abd": [
        ("0eaf0d20311e24abd", "7363616c616c7a", 1699358580000, "mkdir /tmp/test4"),
        ("0eaf0d20311e24abd", "7363616c616c7b",
         1699358680000, "touch /tmp/test4/test.txt"),
        ("0eaf0d20311e24abd", "7363616c616c7c",
         1699358780000, "rm -rf /tmp/test4"),
    ],
    "183ca5d3a2f6735bf": [
        ("183ca5d3a2f6735bf", "7363616c616c7d", 1699359580000, "mkdir /tmp/test5"),
        ("183ca5d3a2f6735bf", "7363616c616c7e",
         1699360580000, "touch /tmp/test5/test.txt"),
        ("183ca5d3a2f6735bf", "7363616c616c7f",
         1699361580000, "rm -rf /tmp/test5"),
    ],
    "640b0507b542f5854": [
        ("640b0507b542f5854", "7363616c616c80", 1699657890000, "mkdir /tmp/test6"),
        ("640b0507b542f5854", "7363616c616c81",
         1699658890000, "touch /tmp/test6/test.txt"),
        ("640b0507b542f5854", "7363616c616c82",
         1699659890000, "rm -rf /tmp/test6"),
    ],
}

# 746f746f2e747874a (resource 1):
#     - n1
# e539b187571ec476a (resource 2):
#     - n1
#     - n2
# 3ad06e1ea9a9c2085 (resource 3):
#     - n1
#     - n2
# 0eaf0d20311e24abd (resource 4):
#     - n1
#     - n3
# 183ca5d3a2f6735bf (resource 5):
#     - n1
#     - n3
# 640b0507b542f5854 (resource 6):
#     - n1
#     - n4

def init_database():
    with sqlite3.connect(db_file) as db_conn:
        current_node = os.environ.get('NODE_FILE')
        # check if tables "state", "resources" and "commands" exist
        cursor = db_conn.cursor()
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND (name='state' OR name='resources' OR name='commands')")
        tables = cursor.fetchall()
        cursor.close()

        if len(tables) == 3:
            print("Database already initialized")
            return

        # create tables
        cursor = db_conn.cursor()
        # state (id (autoincrement), state)
        print("Creating table state")
        cursor.execute(
            "CREATE TABLE state (id INTEGER PRIMARY KEY AUTOINCREMENT, state TEXT)")
        # resources (id, name, last_update)
        print("Creating table resources")
        cursor.execute(
            "CREATE TABLE resources (id TEXT PRIMARY KEY, name TEXT, last_update INTEGER)")
        # precense (resourceId, node_address)
        print("Creating table presence")
        cursor.execute(
            "CREATE TABLE presence (resource_id TEXT, node_address TEXT, PRIMARY KEY (resource_id, node_address), FOREIGN KEY (resource_id) REFERENCES resources(id))") 
        # commands (resource_id, id, create_date, command)
        print("Creating table commands")
        cursor.execute("CREATE TABLE commands (resource_id TEXT, id TEXT, create_date INTEGER, command TEXT, PRIMARY KEY (resource_id, id), FOREIGN KEY (resource_id) REFERENCES resources(id))")

        # insert data
        if current_node == 'node1':
            insert_node1(cursor, db_conn)
        elif current_node == 'node2':
            insert_node2(cursor, db_conn)
        elif current_node == 'node3':
            insert_node3(cursor, db_conn)
        elif current_node == 'node4':
            insert_node4(cursor, db_conn)
        else:
            insert_other(cursor, db_conn)

        cursor.close()


def init_queue_db():
    with sqlite3.connect(db_queue_file) as db_conn:
        # check if table "queue" exists
        cursor = db_conn.cursor()
        cursor.execute(
            "SELECT name FROM sqlite_master WHERE type='table' AND name='resource_queue' OR name='command_queue'")
        tables = cursor.fetchall()
        cursor.close()

        if len(tables) == 2:
            print("Queue database already initialized")
            return

        # create table
        cursor = db_conn.cursor()
        # queue (id (autoincrement), resource_id, command, date, status)
        print("Creating table resource_queue")
        cursor.execute(
            "CREATE TABLE resource_queue (id INTEGER PRIMARY KEY AUTOINCREMENT, node_address TEXT, resource_object TEXT)")
        print("Creating table command_queue")
        cursor.execute(
            "CREATE TABLE command_queue (id INTEGER PRIMARY KEY AUTOINCREMENT, node_address TEXT, resource_id TEXT, command_object TEXT)")
        db_conn.commit()

precense_insert = "insert into presence (resource_id, node_address) values (?, ?)"
state_insert = "insert into state (state) values (?)"
resources_insert = "INSERT INTO resources (id, name, last_update) VALUES (?, ?, ?)"
commands_insert = "INSERT INTO commands (resource_id, id, create_date, command) VALUES (?, ?, ?, ?)"

def insert_node1(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node1")
    cursor.execute(state_insert, (status["node1"]["state"],))
    cursor.executemany(resources_insert, resources)
    
    cursor.executemany(precense_insert, [
        ("746f746f2e747874a", node1_address),
        ("e539b187571ec476a", node1_address),
        ("e539b187571ec476a", node2_address),
        ("3ad06e1ea9a9c2085", node1_address),
        ("3ad06e1ea9a9c2085", node2_address),
        ("0eaf0d20311e24abd", node1_address),
        ("0eaf0d20311e24abd", node3_address),
        ("183ca5d3a2f6735bf", node1_address),
        ("183ca5d3a2f6735bf", node3_address),
        ("640b0507b542f5854", node1_address),
        ("640b0507b542f5854", node4_address)
    ])
    
    for _, commands_list in commands.items():
        cursor.executemany(commands_insert, commands_list)
    db_conn.commit()

def insert_node2(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node2")
    cursor.execute(state_insert, (status["node2"]["state"],))
    rs = resources[1:3]
    cursor.executemany(resources_insert, rs)
    
    cursor.executemany(precense_insert, [
        ("e539b187571ec476a", node1_address),
        ("e539b187571ec476a", node2_address),
        ("3ad06e1ea9a9c2085", node1_address),
        ("3ad06e1ea9a9c2085", node2_address)
    ])
    for r in rs:
        cursor.executemany(commands_insert, commands[r[0]])
    db_conn.commit()

def insert_node3(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node3")
    cursor.execute(state_insert, (status["node3"]["state"],))
    rs = resources[3:5]
    cursor.executemany(resources_insert, rs)
    
    cursor.executemany(precense_insert, [
        ("0eaf0d20311e24abd", node1_address),
        ("0eaf0d20311e24abd", node3_address),
        ("183ca5d3a2f6735bf", node1_address),
        ("183ca5d3a2f6735bf", node3_address)
    ])

    cs = commands[rs[0][0]]
    cs.append(("0eaf0d20311e24abd", "7363616c616c8a",
              1699362580000, "ls -l /tmp"))
    cursor.executemany(commands_insert, cs)

    cs = commands[rs[1][0]]
    cs.pop()
    cursor.executemany(commands_insert, cs)
    db_conn.commit()

def insert_node4(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for node4")
    cursor.execute(state_insert, (status["node4"]["state"],))
    rs = resources[5:]
    cursor.executemany(resources_insert, rs)
    
    cursor.executemany(precense_insert, [
        ("640b0507b542f5854", node1_address),
        ("640b0507b542f5854", node4_address)
    ])

    for r in rs:
        cursor.executemany(commands_insert, commands[r[0]])
    db_conn.commit()

def insert_other(cursor: sqlite3.Cursor, db_conn: sqlite3.Connection):
    print("Inserting data for other nodes, just state table")
    state = random.choice([Status.ONLINE.value, Status.OFFLINE.value])
    cursor.execute(state_insert, (state,))
    db_conn.commit()