import random
import time

from consts.numerical_values import (FAILURE_PERCENTAGE, MAX_SLEEP_TIME,
                                     MIN_SLEEP_TIME)


class RandomNetworkFailure(Exception):
    def __init__(self, message="Random network failure 🙂"):
        self.message = message
        super().__init__(self.message)

def current_milli_time():
    return round(time.time() * 1000)

def random_sleep(min_time = MIN_SLEEP_TIME, max_time = MAX_SLEEP_TIME, fail_percentage = FAILURE_PERCENTAGE):
    if random.random() < fail_percentage:
        raise RandomNetworkFailure()
    time.sleep(random.uniform(min_time, max_time))