from enum import Enum

response_offline = {
    "message": "Unauthorized, the status of the mock is \"OFFLINE\", we can't proceed your request."}


class Status(Enum):
    ONLINE = 'ONLINE'
    OFFLINE = 'OFFLINE'
    STARTING = 'STARTING'
    UNKNOWN = 'UNKNOWN'
    STOPPING = 'STOPPING'
    UPDATING = 'UPDATING'
