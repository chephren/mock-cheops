import time

from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from utils.database_utils import get_state
from utils.http_utils import normalize_url, ping

ping_controller = Blueprint('ping_controller', __name__)
api = Api(ping_controller)


class PingController(Resource):
    def get(self):
        if get_state() == Status.OFFLINE:
            return response_offline, 403
    
        node_addresses = request.headers.get("addresses", None)
        print('node_addresses', node_addresses)
        if (node_addresses is None):
            return jsonify({ "reachable": True, "results": { normalize_url(request.host_url): True } })
        node_addresses = node_addresses.split(",")
        node_addresses = list(map(normalize_url, node_addresses))

        if (normalize_url(request.host_url) in node_addresses):
            node_addresses.remove(normalize_url(request.host_url))

        results = ping(node_addresses, normalize_url(request.host_url))
        results[normalize_url(request.host_url)] = True
        return jsonify({ "reachable": True, "results": results })




api.add_resource(PingController, '/ping')
