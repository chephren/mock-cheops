from flask import Blueprint, jsonify
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from utils.database_utils import get_presence, get_state

presences_controller = Blueprint('presences_controller', __name__)
api = Api(presences_controller)


class PresencesResource(Resource):
    def get(self, resource_id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403

        presences = get_presence(resource_id)
        return jsonify(presences)

api.add_resource(PresencesResource, '/resources/<string:resource_id>/presences')
