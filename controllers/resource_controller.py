from typing import Optional

from flask import Blueprint, jsonify, request
from flask_restful import Api, Resource

from consts.status import Status, response_offline
from models.command import Command
from utils.database_utils import (get_presence, get_resource, get_state,
                                  insert_command, update_state)
from utils.http_utils import normalize_url, send_command_to_other_nodes
from utils.time_utils import random_sleep

resource_controller = Blueprint('resource_controller', __name__)
api = Api(resource_controller)


class ResourceResource(Resource):
    def get(self, id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403

        resource = get_resource(id, with_commands=True, with_presence=True)
        if resource:
            return jsonify(resource.to_full_dict(include_presence=True))
        else:
            return {"message": "Resource not found"}, 404

    def post(self, id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403
        node_address = normalize_url(request.host_url)
        update_state(Status.UPDATING, node_address)
        random_sleep(fail_percentage=0)

        request_command: Optional[str] = request.get_json().get('command')
        request_addresses: list[str] = request.get_json().get('addresses', None)
        if (request_addresses is None or len(request_addresses) == 0):
            request_addresses  = get_presence(id)

        if request_command is None:
            update_state(Status.ONLINE, node_address, {"message": "command is missing", "statusCode": 400})
            return {"message": "command is missing"}, 400

        resource = get_resource(id)
        if resource is None:
            update_state(Status.ONLINE, node_address, {"message": "Resource not found", "statusCode": 404})
            return {"message": "Resource not found"}, 404

        request_addresses = list(map(lambda x: normalize_url(x), request_addresses))

        if len(request_addresses) > 0 and node_address in request_addresses:
            request_addresses.remove(node_address)

        command = Command(request_command)

        # TODO: Potentiellement long, à mettre dans un thread ?
        send_command_to_other_nodes(request_addresses, id, command, source_address=node_address)

        insert_command(id, command)
        update_state(Status.ONLINE, node_address)
        return jsonify(get_resource(id, True, True).to_full_dict(include_presence=True))

    def delete(self, id: str):
        if get_state() == Status.OFFLINE:
            return response_offline, 403
        node_address = normalize_url(request.host_url)
        update_state(Status.UPDATING, node_address)

        random_sleep(fail_percentage=0)
        request_addresses: list[str] = request.get_json().get('addresses', [])
        request_addresses = list(map(lambda x: normalize_url(x), request_addresses))

        resource = get_resource(id)
        if resource is None:
            update_state(Status.ONLINE, node_address, {"message": "Resource not found", "statusCode": 404})
            return {"message": "Resource not found"}, 404

        if len(request_addresses) > 0 and node_address in request_addresses:
            request_addresses.remove(node_address)

        command = Command("DELETE")

        # TODO: Potentiellement long, à mettre dans un thread ?
        send_command_to_other_nodes(request_addresses, id, command, source_address=node_address)

        insert_command(id, command)
        update_state(Status.ONLINE, node_address)
        return jsonify(get_resource(id, True, True).to_full_dict(include_presence=True))


api.add_resource(ResourceResource, '/resource/<string:id>')
